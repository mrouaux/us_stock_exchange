import React, { Component } from 'react'

class TableInfo extends Component {

    render() {
        let open = this.props.companyInfo.open
        let lastClose = this.props.companyInfo.lastClose
        return (
            <div>
                <h5 style={{ display: this.props.companyInfo.companyName === '' ? 'block' : 'none' }}>Please, select a Company on the left side.</h5>
                <div className="row" style={{ display: this.props.companyInfo.companyName === '' ? 'none' : 'block' }}>
                    <table className="highlight">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Open</th>
                                <th>High</th>
                                <th>Low</th>
                                <th>Close</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{this.props.companyInfo.companyName}</td>
                                <td>{this.props.companyInfo.open}</td>
                                <td>{this.props.companyInfo.high}</td>
                                <td>{this.props.companyInfo.low}</td>
                                <td>{this.props.companyInfo.close}</td>
                            </tr>
                        </tbody>
                    </table>

                    <p style={{ color: open > lastClose ? 'green' : 'red' }}>
                        The price has {open > lastClose ? 'increased ' : 'decreased '}
                        by {((open - lastClose) / 100).toFixed(2)}% (USD {(open - lastClose).toFixed(2)})
                    </p>
                </div>
            </div>
        )
    }
}
export default TableInfo