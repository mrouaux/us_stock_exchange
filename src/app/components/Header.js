import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
            <nav>
                <div className="nav-wrapper teal lighten-2">
                    <a href="#" className="brand-logo center">US Stock Exchange</a>
                </div>
            </nav>
        )
    }
}
export default Header