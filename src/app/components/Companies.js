import React, { Component } from 'react'

class Companies extends Component {

    constructor(){
        super()
        this.getCompanySymbol = this.getCompanySymbol.bind(this)
    }


    getCompanySymbol(e){
        this.props.handleShowSymbol(e.target.name)
    }

    render() {
        return (
            <div className="row">
                <div className="card">
                    <div className="card-content">
                        <div className="collection" onClick={this.getCompanySymbol}>
                            <a name="FB" className="collection-item">Facebook (FB)</a>
                            <a name="AAPL" className="collection-item">Apple (AAPL)</a>
                            <a name="MSFT" className="collection-item">Microsoft (MSFT)</a>
                            <a name="GOOGL" className="collection-item">Google (GOOGL)</a>
                            <a name="AMZN" className="collection-item">Amazon (AMZN)</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Companies