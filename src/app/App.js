import React, { Component } from 'react'
import Header from './components/Header'
import Companies from './components/Companies'
import TableInfo from './components/TableInfo'


class App extends Component {

    constructor() {
        super()
        this.state = {
            companyName: '',
            open: '',
            high: '',
            low: '',
            close: '',
            lastClose: ''
        }
    }

    showInfo(companySymbol) {

        let newDate = new Date()
        let year = newDate.getFullYear()
        let month = newDate.getMonth() + 1
        month = month < 10 ? '0' + month : '' + month
        let day = newDate.getDate() - 3
        let previousDay = newDate.getDate() - 4
        let date = year + '-' + month + '-' + day
        let previousDate = year + '-' + month + '-' + previousDay
        console.log(date)
        switch (companySymbol){
            case 'FB': this.state.companyName = 'Facebook'
            break
            case 'AAPL': this.state.companyName = 'Apple'
            break
            case 'MSFT': this.state.companyName = 'Microsoft'
            break
            case 'GOOGL': this.state.companyName = 'Google'
            break
            case 'AMZN': this.state.companyName = 'Amazon'
            break
        }
        fetch('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=' + companySymbol + '&outputsize=compact&apikey=X86NOH6II01P7R24')
            .then(res => res.json())
            .then(data => {
                const lastUpdate = data['Time Series (Daily)'][date]
                const beforeLastUpdate = data['Time Series (Daily)'][previousDate]
                this.setState({
                    open: lastUpdate['1. open'],
                    high: lastUpdate['2. high'],
                    low: lastUpdate['3. low'],
                    close: lastUpdate['4. close'],
                    lastClose: beforeLastUpdate['4. close']
                })
            })
    }

    render() {

        return (
            <div>
                <Header />
                <div className="row" style={{margin: 40}}>
                    <div className="col l4 offset-l1">
                        <Companies
                            handleShowSymbol={this.showInfo.bind(this)}
                        />
                    </div>
                    <div className="col l5 offset-l1">
                        <TableInfo
                            companyInfo={this.state}
                        />
                    </div>
                </div>
            </div>
        )
    }
}
export default App